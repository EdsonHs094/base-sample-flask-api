Install

### Instalar Aplicacion

Entrar en la carpeta app
```sh
$ cd app
```
Instalar las dependencias
```sh
$ pip3 install -r requeriments.txt
```

De no tener pip3 instalar
```sh
$ sudo apt-get install python3-pip
```

Para ejecutar la aplicacion corremos el comando
```sh
$ gunicorn wsgi:app
```
