from flask import Flask, jsonify
from flask_sqlalchemy import SQLAlchemy


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://%s:%s@%s:%s/%s?charset=utf8' % ('root',
                                                                  '123456',
                                                                  '10.60.65.240',
                                                                  '3306',
                                                                  'db_notes')
db = SQLAlchemy(app)

class Notes(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    message = db.Column(db.String(96), unique=False)
    author = db.Column(db.String(96), unique=False)
    name = db.Column(db.String(96), unique=False)


@app.route("/")
def server_info():
    return jsonify({
        "server": "My API"
    })

@app.route("/notes/", endpoint="lista_notes", methods=["GET"])
def list_notes():
    notes = Notes.query.order_by(Notes.id).all()

    return jsonify({
        "items": [{"id": x.id, "message": x.message, "author": x.author, "name": x.name} for x in notes]
    })

if __name__ == "__main__":
    app.run(port=3000, host="0.0.0.0")